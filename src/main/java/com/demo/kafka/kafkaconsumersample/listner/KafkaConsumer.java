package com.demo.kafka.kafkaconsumersample.listner;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {
    @KafkaListener(topics = "messagetopic", groupId = "group1")
    public void consume(String message) {
        System.out.println("Got Message: " + message);
    }
}
